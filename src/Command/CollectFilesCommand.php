<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

/**
 * Class CollectFilesCommand
 * @package App\Command
 */
class CollectFilesCommand extends AbstractRedisCommand
{
    protected function configure()
    {
        $this
            ->setName('app:collect-files')
            ->setDescription('Search in target directory to collect csv files')
            ->addArgument('search-path', InputArgument::REQUIRED, 'Search directory path')
            ->addArgument('storage-path', InputArgument::REQUIRED, 'Storage file path');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $info = new \SplFileInfo($input->getArgument('search-path'));

        // проверки указанного пути
        if (!$info->isDir()) {
            throw new \InvalidArgumentException('Search path is not a directory');
        }
        if (!$info->isReadable()) {
            throw new \InvalidArgumentException('Search path is not readable');
        }

        /** @var Finder $finder */
        // поиск csv файлов в указанной директории использую symfony finder
        $finder = Finder::create()->files()->name('*.csv')->in($info->getRealPath());

        $handler = fopen($input->getArgument('storage-path'), 'wb');
        if ($handler === false) {
            throw new \RuntimeException('Unable to open/create storage file');
        }

        // блокировка файла на время работы с ним
        flock($handler, LOCK_EX);

        $files = [];

        foreach ($finder as $file) {
            $output->writeln('file path: ' . $file->getRelativePathname());

            $files[] = $file->getRealPath();
        }

        // запись списка в файл
        fwrite($handler, implode(PHP_EOL, $files));

        // снятие блокировки и закрытие файлового дескриптора
        flock($handler, LOCK_UN);
        fclose($handler);
    }
}