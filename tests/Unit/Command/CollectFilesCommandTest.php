<?php

namespace App\Tests\Unit\Command;

use App\Tests\Unit\CommandTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CollectFilesCommandTest extends CommandTestCase
{
    public function testExecute()
    {
        $command = $this->app->find('app:collect-files');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'  => $command->getName(),
            'search-path' => realpath(__DIR__ . '/../../resources/target-directory'),
            'storage-path' => realpath(__DIR__ . '/../../resources/target-directory/storage.txt'),
        ]);

        $output = $commandTester->getDisplay();

        $expected = '';
        $expected .= 'file path: dir_1' . DIRECTORY_SEPARATOR . 'dir_1_1' . DIRECTORY_SEPARATOR . 'file_1_1_2.csv' . PHP_EOL;
        $expected .= 'file path: dir_2' . DIRECTORY_SEPARATOR . 'file_2_1.csv' . PHP_EOL;

        $this->assertSame($expected, $output);
    }
}