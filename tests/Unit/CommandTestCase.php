<?php

namespace App\Tests\Unit;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class CommandTestCase extends KernelTestCase
{
    /**
     * @var Application
     */
    protected $app;

    protected function setUp(): void
    {
        $kernel = static::createKernel();
        $this->app = new Application($kernel);
    }
}