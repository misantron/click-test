<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use SymfonyBundles\RedisBundle\Redis\ClientInterface;

/**
 * Class AbstractRedisCommand
 * @package App\Command
 */
abstract class AbstractRedisCommand extends Command
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;

        parent::__construct();
    }
}