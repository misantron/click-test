<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProcessFileCommand
 * @package App\Command
 */
class ProcessFileCommand extends AbstractRedisCommand
{
    protected function configure()
    {
        $this
            ->setName('app:process-file')
            ->setDescription('Process single csv file from list')
            ->addArgument('storage-path', InputArgument::REQUIRED, 'Storage file path');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // получение одного пути к csv файлу
        $path = $this->getFilePath($input->getArgument('storage-path'));
        // если список пуст - показываем ошибку и прекращаем выполнение
        if ($path === null) {
            $output->writeln('<error>Target files list is empty</error>');
            exit(0);
        }

        // пробуем открыть файл на чтение
        $handler = fopen($path, 'rb');
        if ($handler === false) {
            throw new \RuntimeException('Unable to read target file');
        }

        // создаем блокировку на файл, чтобы избежать его внешнего изменения
        flock($handler, LOCK_EX);

        // читаем заголовок из файла
        $headers = fgetcsv($handler, 4096, ';');

        foreach ($this->readLine($handler, $headers) as $item) {
            // сохраняем дату в отдельный список
            $this->client->sadd('dates', $item['date']);

            // агрегируем данные по колонкам-метрикам в хранилище
            $this->client->incrby('metrics:A:' . $item['date'], $item['A']);
            $this->client->incrby('metrics:B:' . $item['date'], $item['B']);
            $this->client->incrbyfloat('metrics:C:' . $item['date'], $item['C']);
        }
    }

    /**
     * Extract first csv file path from list stored in file
     *
     * @param string $storagePath
     * @return string|null
     */
    private function getFilePath(string $storagePath): ?string
    {
        // читаем содержимое файла в массив
        $rows = file($storagePath);
        // если список пуст - возращаем null
        if (empty($rows)) {
            return null;
        }

        // получаем первую строку из массива
        $path = array_shift($rows);

        // остальные строки сохраняем обратно в файл
        file_put_contents($storagePath, implode(PHP_EOL, $rows));

        return $path;
    }

    /**
     * Generator function read csv file by lines
     *
     * @param resource $handler
     * @param array $headers
     * @return \Generator
     */
    private function readLine($handler, array &$headers): \Generator
    {
        try {
            // читаем содержимое файла построчно, не загружая его в память целиком
            while ($row = fgetcsv($handler, 4096, ';')) {
                // из csv строки формируем ассоциативный массив
                yield array_combine($headers, $row);
            }
        } finally {
            // после того как чтение закончено - снимаем блокировку и закрываем соединение
            flock($handler, LOCK_UN);
            fclose($handler);
        }
    }
}