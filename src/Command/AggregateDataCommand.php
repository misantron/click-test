<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AggregateDataCommand
 * @package App\Command
 */
class AggregateDataCommand extends AbstractRedisCommand
{
    protected function configure()
    {
        $this
            ->setName('app:aggregate-data')
            ->setDescription('Aggregate data from csv sources in single file')
            ->addArgument('target-path', InputArgument::REQUIRED, 'Aggregated data file path');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // получаем список дат из хранилища
        $dates = $this->client->smembers('dates');
        // если список пустой - показываем ошибку и завершаем задачу
        if (empty($dates)) {
            $output->writeln('<error>Dates list is empty</error>');
            exit(0);
        }

        $metrics = ['A', 'B', 'C'];
        $headers = $metrics;
        array_unshift($headers, 'date');

        // пробуем открыть целевой файл на запись
        $handler = fopen($input->getArgument('target-path'), 'wb');
        if ($handler === false) {
            throw new \RuntimeException('Unable to open target file');
        }

        // создаем блокировку файла
        flock($handler, LOCK_EX);

        // записываем в файл имена колонок
        fputcsv($handler, $headers, ';');

        foreach ($dates as $date) {
            $row = [$date];
            // формируем из агрегированный данных csv строку
            foreach ($metrics as $name) {
                $row[] = $this->client->get('metrics:' . $name . ':' . $date);
            }
            // записываем строку в файл
            fputcsv($handler, $row, ';');
        }

        // снимаем блокировку и закрываем соединение
        flock($handler, LOCK_EX);
        fclose($handler);
    }
}